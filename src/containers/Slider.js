import React , {useState} from 'react';
import './slider.css';
import FrontSlider from './Frontslider';

class Slider extends React.Component{

    constructor(props){
        super(props)
        this.state={
            
        }
    }

    render(){
        return (
          <div className="slider-main-div">
            <div className="slider-main-pic">
              <img alt='' className='slider-img' src={require("../assets/img/HomeHeader.d4396cb2.jpg")} />
            </div>
            <div className="slider " >

            <FrontSlider/>

            </div>
          </div>
        );
    }

}

export default Slider