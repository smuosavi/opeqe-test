
import React, { useState } from "react";
import Carousel from "react-bootstrap/Carousel";

export default   function FrontSlider() {
  const [index, setIndex] = useState(0);
  const [direction, setDirection] = useState(null);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
    setDirection(e.direction);
  };

  return (
    <Carousel activeIndex={index} direction={direction} onSelect={handleSelect}>
      <Carousel.Item>
        <Carousel.Caption>
          <h1>Chief Special</h1>
          <p> Get $10 off when you order $20 or more T-Bone Steak & Eggs</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <Carousel.Caption>
          <h1>code FREEDINE</h1>
          <p>
            {" "}
            Enjoy $10 Off When you order two or more of Blueberry Pancake
            Breakfast The Coupon is only applicable for the specific item and
            can not be combine with any other orde
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}
