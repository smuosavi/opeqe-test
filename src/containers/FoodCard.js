import React from 'react'

class FoodCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

  }
//  src={require("../assets/img/3.jpg")}
  render(){
      return (
        <div className="list-card">
          <div className="list-card-container">
            <div className="list-inner-card">
              <div className="list-card-pic-div">
                <a className="list-card-pic-link">
                  <img
                    alt=""
                    className="list-card-img"
                    src={`${this.props.data.image}`}
                  />
                  <span className="list-card-img-text"> Chief Special </span>
                </a>
              </div>
              <div className="list-card-info-container">
                <div className="list-card-info-title">
                  {`${this.props.data.title}`}
                </div>
                <div className="list-card-info-subtitle">
                  <div className="list-card-info-subtitle-firstlink">
                    {this.props.data.menuType.title}
                  </div>
                  <div className="list-card-info-subtitle-seclink">
                    {this.props.data.cuisineType.title}
                  </div>
                  <div className="list-card-info-subtitle-seclink">
                    {this.props.data.courseType.title}
                  </div>
                </div>
                <div className="list-info-time-div">
                  <span>
                    <span className="info-time-timelight">
                      {this.props.data.preparation} Min
                    </span>
                    <span className="info-time-info-dark">
                      {this.props.data.price}
                    </span>
                  </span>
                  <span className="info-time-info-service">
                    {" "}
                    Free Delivery{" "}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
  }
}
export default FoodCard