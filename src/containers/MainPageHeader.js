import React from "react";

class MainPageHeader extends React.Component {
    constructor(props){
        super(props);
        this.state={
            
        }
    }

    render(){
        return (
          <div className="main-page-header">
            <div className="main-header-contents">
              <div className="header-address-div">
                <div className="header-address-title">ASAP Delivery </div>
                <div className="header-address-btn"> What's your address</div>
              </div>
              <div className="header-change-div">
                <a className="header-change-btn">
                  <span className="header-change-btn-text"> change</span>
                  <span className="header-change-btn-body"></span>
                </a>
              </div>
              <div className="header-servicetype-div">
                <div className="heaer-servicetype-underline"></div>
                <div className="heaer-servicetype-type"> Delivery </div>
                <span className="heaer-servicetype-or"> or </span>
                <div className="heaer-servicetype-type"> Pick up </div>
              </div>
              <div className="heaer-servicetype-divider"></div>
              <a className="heaer-servicetype-link"> Catering </a>
            </div>
          </div>
        );
    }

}
export default MainPageHeader;
