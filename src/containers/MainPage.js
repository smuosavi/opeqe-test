import React from 'react';
import './mainpage.css';
import FoodCard from './FoodCard';
import MainPageHeader from './MainPageHeader'
const data = require("../data.json"); 
class MainPage extends React.Component{

    constructor(props){
        super(props);
        this.state={
        
        }
    }

    render(){

        return (
          <div className="main-page-body">
            {/* header */}
            <MainPageHeader />

            {/* raws */}
            <div className="raws">
              <div className="inner-raws">
                <div className="raw-list">
                  <div className="raw-list-title">
                    <div className="list-title"> Special Offers </div>
                    <div className="list-title-b"></div>
                  </div>

                  <div className="list-title-underline">
                    <div className="list-line-bar">
                      <div className="list-line-barc"></div>
                    </div>
                  </div>
                  <div className="list-slider">
                    <div className="list-container">
                      <div className="list-inner-container">
                        {data.map(data => {
                          return <FoodCard data={data} />;
                        })}

                        {/* card */}
                      </div>
                    </div>
                    <div className="list-slider-last"></div>
                    <div className="list-slider-next">
                      <button className="list-slider-nextbtn">
                        <span className="nextbtn-body">
                          <i class="fa fa-angle-right"></i>
                        </span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="raw-about">
              <div className="raw-about-img-div">
                <img
                  alt=""
                  className="raw-about-img"
                  src={require("../assets/img/gift-card.jpg")}
                />
              </div>
              <div className="raw-about-text-div">
                <div className="about-upper-title">
                  <span></span>
                  <span></span>
                </div>
                <div className="about-logo-div"></div>
                <div className="about-des-div">
                  Opeqe provides a wide range of customizable products for
                  reward and incentive programs that can meet your restaurant’s
                  goals.
                  <br />
                  Whether you are looking to drive the addition of new
                  customers, increase the loyalty of existing ones we have a
                  customized solution for you.
                </div>
                <div className="about-btn-div">
                  <div className="about-btn-innerdiv">
                    <button className="about-btn">
                      <span className="about-btn-label"> GET ONLINE QOUTE</span>
                      <span className="about-btn-body"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}
export default MainPage