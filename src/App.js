import React from 'react'

import Navbar from "./containers/Navbar";
import Slider from './containers/Slider.js';
import MainPage from './containers/MainPage';
import './containers/header.css'

 
class App extends React.Component {

    constructor(props){
        super(props);
        this.state={
          
        }
//  console.log("this is the data comming", data);
    }

   
    render(){
        return (
          <div>
            <header className="header-container">
              <div className="header-container-div">
                <div className="header-innercontainer">
                  <span className="header-leftpart">
                    <button className=""></button>
                  </span>
                  <span className="header-rightpart">
                    <a className="header-linktext"> Reservation </a>
                    <a className="header-linktext"> Orders</a>
                    <div className="header-login-div">
                      <a className="header-login-a">
                        <span className="header-login-text">login</span>
                        <span className="header-login-body"></span>
                      </a>
                    </div>
                    <div className="header-sign-up-div">
                      <a className="header-sign-up-a">
                        <span className="header-sign-up-text"> sign up</span>
                        <span className="header-sign-up-body"></span>
                      </a>
                    </div>
                    <a className="shop-logo"></a>
                  </span>
                </div>
              </div>
            </header>

            {/* <Navbar/> */}
            <Slider />
            <MainPage />
          </div>
        );
    }

}

export default App;